#!/bin/bash
pip3 install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic --noinput
gunicorn --bind :3000 WinterBlog.wsgi:application
python3 manage.py runserver 0.0.0.0:8000

