# WinterAssignment
What this project is, is essentially a social blog application created in django where users can maintain their profile, look at others profiles, follow or unfollow others, post their own blog-posts, comment on blog posts, etc.

The project also has an extra feature for admins. Admins have been provided with a clean interface for generating excel sheets or pdfs containing details of all the current users as well as banning/un-banning other non-administrative users.

## Dependencies
- Python 2.7
- Django 1.11
- Pillow 3.4.0
- Xlsxwriter 1.0.2
- Reportlab 3.4.0

## How to Setup:
1. Install virtualenv if you don't already have it, then make a virtual environment like so:
```bash
$ sudo pip install virtualenv
$ mkdir winterblogve
$ virtualenv winterblogve
```

2. activate your new virtualenv then go to the source folder of the WinterAssignment.
```bash
$ source winterblogve/bin/activate
$ cd WinterAssignment
```

3. Install all of the dependencies.
```bash
$ pip install -r requirements.txt
```

4. Finally, migrate code, collect static files, then run the development server.
```bash
$ python manage.py migrate
$ python manage.py runserver
```

## Notes:
This project is running on simple development-grade code. If you wanted to run it in production, you would have to make several modifications including:
1. Changing to a more production-grade database such as MySQL or PostgreSQL.
2. Use a proper production-grade web server (e.g. using nginx+gunicorn or apache+uwsgi or any other combination you find suits your needs well).
3. Serve static and media files via. the web server as opposed to Django's development server.
4. Turn debug mode off (i.e. do ```DEBUG = False```).
5. Keep project related secrets (e.g. ```SECRET_KEY```) in a seperate python file or better yet, set them as environment variables and then fetch then using python's _os library_. Remove any secrets from the version control (e.g. gitignore them).

etc.

<hr>

***Finally, I would like to mention that I made this when I was still new to Django in the Winter (December) of 2017. I've learnt a lot more since then but this is a nice reminder to me that "The expert in anything was once a beginner" (- Helen Hayes).***

<hr>
