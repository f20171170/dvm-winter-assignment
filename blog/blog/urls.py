from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from . import views

app_name="blog"
urlpatterns = [
	url(r'^admin_tools/$', views.admin_tools, name="admin_tools"),
	url(r'^alter/$', views.alter, name="alter"),
	url(r'^post/(?P<post_id>[0-9]+)', views.comment, name="comment"),
	url(r'^delete/(?P<type>[post|comment]+)', views.delete, name="delete"),
	url(r'^discover/$', views.discover, name="discover"),
	url(r'^feed/$', views.feed, name="feed"),
	url(r'^follow/(?P<username>[a-zA-Z0-9_.+-]+)$', views.follow, name="follow"),
	url(r'^profile/edit/(?P<username>[a-zA-Z0-9_.+-]+)/$', views.edit, name="edit"),
	url(r"^$", views.home, name="home"),
	url(r'^signup/$', views.signup, name="signup"),
	url(r'^pdf/$', views.pdf, name="pdf"),
	url(r'^post/$', views.post, name="post"),
	url(r'^profile/(?P<username>[a-zA-Z0-9_.+-]+)/$', views.profile, name="profile"),
	url(r'^login/$', views.ulogin, name="ulogin"),
	url(r'^unfollow/(?P<username>[a-zA-Z0-9_.+-]+)$', views.unfollow, name="unfollow"),
	url(r'^xlsx/$', views.xlsx, name="xlsx"),
	]

# This will allow us to add static and media urls as well as specify where files should be uploaded to.
# DEBUG == True requirement because this is unsuitable for production.
statics = static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
medias = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
extras = statics + medias
if settings.DEBUG == True:
    urlpatterns += extras
