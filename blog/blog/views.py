                                # VIEW IMPORTS

# For python 2 vs 3 compatibility
from __future__ import unicode_literals

# Standard Library Imports
import re
import os
import io
import logging
from datetime import datetime

# Django Settings and Functions to Return to clients
from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

# Django Authorization
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Django Messages
from django.contrib import messages

# Code That I Wrote
from .models import Blogger, Post, Comment
from .externalfunctions import new_user_invalid, ordered_feed, generate_xlsx, generate_pdf, inactive_validation


                                # SET UP LOGGING SYSTEM

INFOPATH = os.path.join(settings.BASE_DIR, "blog/logs/INFO.log")
DEBUGPATH = os.path.join(settings.BASE_DIR, "blog/logs/DEBUG.log")
LOG_SETTINGS = {
    'version':1,
    'disable_existing_loggers':False, # to keep the Django logging system
    'formatters':{
        'default': {
            'format':'TIME: %(asctime)s\nLEVEL: %(levelname)s\nMESSAGE: %(message)s\n'
        }
    },
    'handlers':{
        'info':{
            'level':'INFO',
            'formatter':'default',
            'class':'logging.FileHandler',
            'filename':INFOPATH
        },
        'debug':{
            'level':'DEBUG',
            'formatter':'default',
            'class':'logging.FileHandler',
            'filename':DEBUGPATH,
            'mode':'w'
        }
        # until and unless someone adds some debug messages, the two will be the same.
    },
    'loggers':{
        'viewlog':{
            'level':'INFO',
            'handlers':['info','debug'],
            'propagate':True
        }
    }
}


logging.config.dictConfig(LOG_SETTINGS)
viewlog = logging.getLogger("viewlog")

"""
if you are getting any errors regarding the logging system, first make sure that
the files/folder: <BASE_DIR>/blog/logs/DEBUG.log and <BASE_DIR>/blog/logs/DEBUG.log exist,
else, make them.
"""

                                # VIEW FUNCTIONS


# Visible view where users can login or sign up
def home(request):
    if request.user:
        logout(request)
        viewlog.info("User {} has logged out.".format(request.user.username))
    context = None
    return render(request, 'blog/home.html', context)


# Hidden view for signing up new users and logging them in
def signup(request):
    username = request.POST['username'].lower()
    password = request.POST['password']
    p_verification = request.POST['password_verifier']
    email = request.POST['email_id']

    credentials = [username, password, p_verification, email]

    invalid = new_user_invalid(credentials)

    if not invalid:
        u = User(username=username, email=email)
        u.set_password(password)
        u.save()

        b = Blogger(user=u)
        b.save()

        user = authenticate(username=u.username, password=u.password)
        login(request, user)

        return redirect('blog:profile', permanent=True, username=user.username)

    else:
        messages.add_message(request, messages.WARNING, invalid)
        return redirect('blog:home')


# Hidden view for logging users in
def ulogin(request):
    data = request.POST
    username = data['username'].lower()
    password = data['password']

    # SQL injection is already taken care of by Django, this is just an easter egg.
    if username == "1'or'1'='1":
        messages.add_message(request, messages.ERROR, 'lol, nice try turd.')
        viewlog.warning("An idiot tried to log in.")
        return redirect('blog:home')

    user = authenticate(username=username, password=password)

    if user == None:
        if inactive_validation(username=username, password=password):
        # the user is in the db but is currently 'inactive' i.e. banned.
            messages.add_message(request, messages.ERROR, 'You have been banned!!!')
            return redirect('blog:home')
        else:
        # user not in the db/credentials are wrong.
            messages.add_message(request, messages.ERROR, 'invalid login credentials!')
            viewlog.warning("Failed login attempt for username: {}".format(username))
            return redirect('blog:home')
    else:
        login(request, user)
        viewlog.info("Successful login attempt for username: {}".format(username))
        return redirect('blog:profile', permanent=True, username=user.username)


# Visible view for the user's posts feed
@login_required
def feed(request):
    feedable =  []
    for u in request.user.blogger.following.all():
        for p in u.post_set.all():
            feedable.append(p)
    feed = ordered_feed(feedable)
    context = {"feed":feed}
    viewlog.info("User: {} visited their feed".format(request.user.username))
    return render(request, "blog/feed.html", context)


# Visible view for the user's profile page
@login_required
def profile(request, username):
    profile_person = User.objects.get(username=username)
    joined = profile_person.date_joined.strftime("%b %d %Y")
    context = {"user":profile_person, "posts":profile_person.post_set.order_by('-date')}
    viewlog.info("User: {} visited their profile".format(request.user.username))
    return render(request, 'blog/profile.html', context)


# Visible view for allowing the user to edit their profile
@login_required
def edit(request, username):
    if request.method == "GET":
    # give the user a fresh page
        if request.user.username == username:
            viewlog.info("User: {} has opened their profile edit panel".format(request.user.username))
            return render(request, 'blog/edit.html', None)
        else:
        # don't allow users to edit other users' profiles
            if not request.user.is_superuser:
                messages.add_message(request, messages.WARNING, "You are not allowed to edit other's profiles! The admin has been notified of this event.")
                viewlog.warning("User: {} has attempted to edit {}'s profile!!!".format(request.user.username, username))
            return redirect('blog:profile', username=username)

    elif request.method == "POST":
    # make sure that the entered credentials are acceptable and that the user has a blogger table in the database
        data = request.POST
        username = request.user.username
        user = User.objects.get(username=username)

        try:
        # this will raise an error if the blogger table doesn't exist
            user.blogger.first_name = data['first_name'].title()

        except:
            b = Blogger.objects.get_or_create(user=user)

        finally:
            if data['first_name']:
                user.blogger.first_name = data['first_name'].title()
            else:
                user.blogger.first_name = ""

            if data['last_name']:
                user.blogger.last_name = data['last_name'].title()
            else:
                user.blogger.last_name = ""

            if data['bhawan']:
                user.blogger.bhawan = data['bhawan'].title()
            else:
                user.blogger.bhawan = ""

            if data['email'] and re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", data['email'] ):
                user.email = data['email']

            else:
                messages.add_message(request, messages.ERROR, 'you must have a valid email id')
                viewlog.warning("USER: {} has failed to edit their profile. F12 suspected.".format(request.user.username))
                return redirect('blog:edit', request.user.username)


            try:
                if request.FILES["profile_pic"]:
                    f = request.FILES["profile_pic"]
                    user.blogger.dp = f
                    user.blogger.save()
                    user.blogger.resize()
                    # saving once to take f from being an InMemoryUploadedFile object to a saved file in the right folder
            except:
                pass

            finally:
                user.blogger.save()
                user.save()
                viewlog.info("User: {} has successfully edited their profile".format(request.user.username))
                return redirect('blog:profile', request.user.username)


# Hidden view for the follow functionality
@login_required
def follow(request, username):
    to_add = User.objects.get(username=username)
    u = request.user
    u.blogger.following.add(to_add)
    u.save()
    viewlog.info("User: {} has followed {}".format(request.user.username, username))
    return redirect('blog:profile', username)


# Hidden view for the unfollow functionality
@login_required
def unfollow(request, username):
    to_rem = User.objects.get(username=username)
    u = request.user
    u.blogger.following.remove(to_rem)
    u.save()
    viewlog.info("User: {} has unfollowed {}".format(request.user.username, username))
    return redirect('blog:profile', username)


# Visible view for the 'discover other users' functionality
@login_required
def discover(request):
    all_users = User.objects.all()
    all_unfollowed_users = []
    for user in all_users:
        if user not in request.user.blogger.following.all() and user != request.user:
            all_unfollowed_users.append(user)
    context = {"all_unfollowed_users":all_unfollowed_users}
    viewlog.info("User: {} visited their discover page".format(request.user.username))
    return render(request, 'blog/discover.html', context)


# Visible view where the user can write a Post and then post it
@login_required
def post(request):

    if request.method == "GET":
        viewlog.info("User: {} visited the post page".format(request.user.username))
        return render(request, 'blog/post.html')

    if request.method == "POST":
        post_data = request.POST

        p = Post()
        p.title = post_data['title']
        p.content = post_data['content']
        p.user = request.user
        p.save()

        messages.add_message(request, messages.SUCCESS, "successfully posted")
        viewlog.info("User: {} successfully posted (post id{})".format(request.user.username, p.id))
        return redirect('blog:profile', request.user.username)


# Visible view where each post can be viewed independently. Here comments can be added and users can delete their posts and comments
@login_required
def comment(request, post_id):

    if request.method == "GET":
        post = Post.objects.get(pk=post_id)
        if len(post.comments.all()) == 0:
            no_comments = True
        else:
            no_comments = False
        context = {"post":post, "no_comments":no_comments}
        viewlog.info("User: {} visited post #{}".format(request.user.username, post_id))
        return render(request, "blog/comment.html", context)

    elif request.method == "POST":
        comment = request.POST["comment"]
        user = request.user
        post = Post.objects.get(pk=post_id)
        c = Comment(content=comment, user=user, post=post)
        c.save()
        messages.add_message(request, messages.SUCCESS, "successfully posted")
        return redirect('blog:comment', post_id)


# Hidden view where the user can delete their post or comment
@login_required
def delete(request, type):
    if type == "post":
        post_id = request.POST["post_id"]
        p = Post.objects.get(id=post_id)
        title = p.title
        content = p.content
        p.delete()
        viewlog.info("User: {} has deleted their post (id:{})\n    TITLE:{}\n    CONTENT:{} ".format(request.user.username, post_id, title, content))
        return redirect('blog:profile', request.user.username)

    elif type == "comment":
        comment_id = request.POST["comment_id"]
        c = Comment.objects.get(id=comment_id)
        p = c.post.id
        content = c.content
        c.delete()
        viewlog.info("User: {} has deleted their post (id:{})\n    CONTENT:{} ".format(request.user.username, comment_id, content))
        return redirect('blog:comment', p)


# Special Visible view for admins for the Admin Tools page
@user_passes_test(lambda x: x.is_superuser)
def admin_tools(request):
    context = {'users':User.objects.all()}
    viewlog.info("Admin {} visited the Admin Tools Panel (ATP)".format(request.user.username))
    return render(request, "blog/admin_tools.html", context)


# Special Hidden view for generating and downloading or viewing a .xlsx file with all user's credentials
@user_passes_test(lambda x: x.is_superuser)
def xlsx(request):

    output = io.BytesIO()

    name = generate_xlsx(output)

    output.seek(0)

    response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename={}".format(name)

    viewlog.info("Admin {} has generated a spreadsheet".format(request.user.username))
    return response


# Special Hidden view for generating and downloading or viewing a .pdf file with certain user's credentials
@user_passes_test(lambda x: x.is_superuser)
def pdf(request):
    name = datetime.now()
    name = name.strftime('%d%b%Y')

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename={}.pdf'.format(name)

    generate_pdf(response)

    viewlog.info("Admin {} has generated a spreadsheet".format(request.user.username))
    return response


# Special Hidden view for Admins to ban or unban multiple users at once
@user_passes_test(lambda x: x.is_superuser)
def alter(request):
    data = request.POST

    users_unicode_list = []
    for user in User.objects.all():
        users_unicode_list.append(str(user.username))

    active = []
    blocked = []

    for user in data:
        # user is the key but in unicode...
        # print(user) to test
        if user in users_unicode_list:
            usr = User.objects.get(username=user)
            if usr.is_active:
                if not usr.is_superuser:
                    # Ban
                    usr.is_active = False
                    active.append(usr.username)
                    usr.save()
            else:
                # Activate
                usr.is_active = True
                blocked.append(usr.username)
                usr.save()
        else:
            pass

    viewlog.warning("Admin: {} has performed an alteration!\n    ACTIVATED:{}\n    BANNED:{}".format(request.user.username, active, blocked))
    return redirect('blog:admin_tools')
