"""
For the sake of organisation, some functions have been defined in this document
so that the code can be kept clean and easy to read, not overwhelming.
"""

import re
from datetime import datetime
import xlsxwriter
from reportlab.platypus.tables import Table
from reportlab.platypus import SimpleDocTemplate

from django.contrib.auth.models import User

from .models import *


def new_user_invalid(li):
# this code will act as a verifier during sign up to make sure that the entered credentials are acceptable
    # unpack the data passed during the function call
    username = li[0]
    password = li[1]
    password_verifier = li[2]
    email = li[3]

    # this block obtains a list of all usernames in the database at the time of call
    # all of the usernames are u'' or unicode type strings
    usernames = []
    for user in User.objects.all():
        usernames.append(str(user.username))


# Testing begins
    # see if everything exists:
    if not username or not password or not password_verifier or not email:
        err_message = "all fields must be filled, and stay away from f12"
        return err_message

    # test the username:
    if not re.match(r"^[a-zA-Z0-9_.-]+$", username):
        err_message = "username is of an invalid format"
        return err_message

    if username in usernames:
        err_message = "username already exits"
        return err_message

    # test for matching passwords:
    if password != password_verifier:
        err_message = "passwords do not match"
        return err_message

    # test for valid email format:
    if not re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
        err_message="invalid email_id"
        return err_message

    # if everything matches, the user is valid and hence False is returned here
    else:
        return False


def ordered_feed(feed_list):
    # my algorithm for sorting posts of users that the request.user is following, in alphabetical order.
    # I wrote the algorithm myself, and it's certainly not the best.
    f = feed_list
    ordered_f = []
    cursor_pos = 0
    before = cursor_pos - 1
    after = cursor_pos + 1
    for post in f:
        if len(ordered_f) == 0:
            ordered_f.append(post)
        else:
            if post.date > ordered_f[cursor_pos].date:
            # i.e. if the post is newer:
                if before < 0:
                    ordered_f.insert(0, post)
                else:
                    ordered_f.insert(before, post)
            else:
            # if the post is older:
                ordered_f.append(post)
    return ordered_f


def generate_xlsx(output):
# to generate a .xlsx file for the admin tools feature

    # Create name for the workbook
    name = datetime.now()
    name = name.strftime('%d%b%Y')
    name += '.xlsx'

    # Initialize Workbook
    workbook = xlsxwriter.Workbook(output, {'remove_timezone':True, 'in_memory':True})
    worksheet = workbook.add_worksheet("Users")
    worksheet.set_paper(9) # for default:A4
    worksheet.set_column(0,2,15)
    worksheet.set_column(3,3,10)
    worksheet.set_column(4,4,30)
    worksheet.set_column(5,5,25)
    worksheet.set_column(6,6,7)

    bold = workbook.add_format({'bold': True})
    worksheet.write('A1','Username',bold)
    worksheet.write('B1','First Name',bold)
    worksheet.write('C1','Last Name',bold)
    worksheet.write('D1','Bhawan',bold)
    worksheet.write('E1','E-Mail',bold)
    worksheet.write('F1','Date Joined (dd-mm-yyyy)',bold)
    worksheet.write('G1', 'Status',bold)


    # loop through the users
    row = 1
    column = 0
    for user in User.objects.all():
            worksheet.write(row, column, user.username )
            worksheet.write(row, column+1, user.blogger.first_name)
            worksheet.write(row, column+2, user.blogger.last_name)
            worksheet.write(row, column+3, user.blogger.bhawan)
            worksheet.write(row, column+4, user.email)
            date = user.date_joined
            date = date.strftime('%d-%m-%Y')
            worksheet.write(row, column+5, date)
            if user.is_superuser:
                worksheet.write(row, column+6, "admin")
            else:
                if user.is_active:
                    worksheet.write(row, column+6, "Active")
                else:
                    worksheet.write(row, column+6, "Banned")
            row += 1

    # close the workbook
    workbook.close()

    # return the name
    return name


def generate_pdf(response):
# to generate a .pdf file for the admin tools feature

    cm = 2.54
    elements = []
    doc = SimpleDocTemplate(response, rightMargin=2.5*cm, leftMargin=2.5*cm, topMargin=0.5*cm, bottomMargin=0)

    data=[("USERNAME", "FIRST NAME", "LAST NAME", "BHAWAN", "E-MAIL")]

    for user in User.objects.all():
        date = user.date_joined
        date = date.strftime('%d-%m-%Y')
        li = (user.username, user.blogger.first_name, user.blogger.last_name, user.blogger.bhawan, user.email)
        data.append(li)

    table = Table(data, colWidths=100, rowHeights=100)
    elements.append(table)
    doc.build(elements)


def inactive_validation(username,password):
# the authenticate() function will return None even if the user exists but is inactive
# this function will return True if the user is in the db but inactive
    for user in User.objects.all():
        try:
            u = User.objects.get(username=username)
            if password == u.blogger.password:
            # the user exists, creds are right but is banned
                return True
            else:
            # invalid credentials
                return False
        except:
            pass
        return False
