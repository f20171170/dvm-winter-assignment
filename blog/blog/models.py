# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from PIL import Image

from django.db import models
from django.conf import settings
from django.core.files import File
from django.contrib.auth.models import User


# this function is passed to the upload_to argument of the dp ImageField of Blogger
# to allow each user to have their own folder for DPs.
def user_directory_path(instance, filename):
    return "{0}/{1}".format(instance.user.username, filename)


class Blogger(models.Model):
    user = models.OneToOneField(User, models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True, default="")
    last_name = models.CharField(max_length=100,  blank=True, default="")
    bhawan = models.CharField(max_length=30, blank=True, default="")
    following = models.ManyToManyField(User, related_name="following")
    dp = models.ImageField(upload_to=user_directory_path, default="db.jpeg")
    # username and email are already covered by the default user class i.e. the one-to-one relation here.

    def __str__(self):
        return self.user.username

    def resize(self):
        # Resize operation to create a 250 x 250 px image.
        i_path = str(os.path.join(settings.MEDIA_ROOT, self.dp.name))
        im = Image.open(i_path)
        im = im.resize((250,250))
        im.save(i_path)


class Post(models.Model):
    date = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=50)
    content = models.TextField(max_length=1000)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Comment(models.Model):
    content = models.TextField(max_length=100)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    # when working with the Django Database API use post.comments instead of post.comment_set,
    # that's what the related name is for!

    def __str__(self):
            return self.post.title
