# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Blogger, Post, Comment


class CommentInline(admin.TabularInline):
    model = Comment
    extra = 0

class PostInline(admin.TabularInline):
    model = Post
    extra = 0

class PostAdmin(admin.ModelAdmin):
    inlines=[CommentInline]


class BloggerAdmin(admin.ModelAdmin):
    fieldsets = [
        ('User Credentials',{'fields':['first_name','last_name','password']}),
    ]
    list_display = ["user", "first_name", "last_name"]

admin.site.register(Blogger, BloggerAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
